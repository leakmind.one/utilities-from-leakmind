<?php
header ("Cache-Control: no-cache, must-revalidate");
session_start([
    'read_and_close'  => false,
]);
$title_array = array(
    'home' => 'HOME',
    'ssl' => 'SSL CHECK',
    'ptr' => 'PTR CHECK',
    'whois' => 'WHOIS',
    'my_ip' => 'INFO YOUR IP',
    'blacklist' => 'CHECK BLACKLIST',
    'ping' => 'PING',
    'dig' => 'DIG',
    'serv_res' => 'SERVER RESPONSE',
    'port' => 'CHECK PORTS',
    'base64' => 'CODE\DECODE base64',
    'geoip' => 'GeoIP',
    'md5' => 'MD5',
    'punycode' => 'Punycode',
    'rkn' => 'Check ip\domain in block Roskomnadzor',
    'telegram_tools' => 'Telegram tools',
    'dwyou' => "Download Video Youtube"
);
$url = substr($_SERVER['REQUEST_URI'] , 1);
?>
<!doctype html>
<html lang="en">
<head>
    <title> <?php if (empty($url)) {echo $title_array['home'];} else { echo $title_array[$url];} ?>  | leakmind.one</title>
    <link rel="icon" href="https://leakmind.one/wp-content/uploads/2021/01/logo-1-150x150.png" sizes="32x32" />
    <link href='https://fonts.googleapis.com/css2?family=Raleway' rel='stylesheet'>
    <link href='./style.css' rel='stylesheet'>
</head>
<body>
<div class='menu'>
<a href='home' <?php if (strpos($_SERVER['REQUEST_URI'], "home") !== false || empty($url)){echo "class='active'";} ?>>HOME</a>
<a href='ssl' <?php if (strpos($_SERVER['REQUEST_URI'], "ssl") !== false){echo "class='active'";} ?>>SSL CHECK</a>
<a href='ptr' <?php if (strpos($_SERVER['REQUEST_URI'], "ptr") !== false){echo "class='active'";} ?>>PTR</a>
<a href='whois' <?php if (strpos($_SERVER['REQUEST_URI'], "whois") !== false){echo "class='active'";} ?>>WHOIS</a>
<a href='my_ip' <?php if (strpos($_SERVER['REQUEST_URI'], "my_ip") !== false){echo "class='active'";} ?>>INFO YOUR IP</a>
<a href='blacklist' <?php if (strpos($_SERVER['REQUEST_URI'], "blacklist") !== false){echo "class='active'";} ?>>CHECK BLACKLIST</a>
<a href='ping' <?php if (strpos($_SERVER['REQUEST_URI'], "ping") !== false){echo "class='active'";} ?>>PING</a>
<a href='dig' <?php if (strpos($_SERVER['REQUEST_URI'], "dig") !== false){echo "class='active'";} ?>>DIG</a>
<a href='serv_res' <?php if (strpos($_SERVER['REQUEST_URI'], "serv_res") !== false){echo "class='active'";} ?>>SERVER RESPONSE</a>
<a href='port' <?php if (strpos($_SERVER['REQUEST_URI'], "port") !== false){echo "class='active'";} ?>>CHECK PORTS</a>
<a href='base64' <?php if (strpos($_SERVER['REQUEST_URI'], "base64") !== false){echo "class='active'";} ?>>CODE\DECODE base64</a>
<a href='geoip' <?php if (strpos($_SERVER['REQUEST_URI'], "geoip") !== false){echo "class='active'";} ?>>GeoIP</a>
<a href='md5' <?php if (strpos($_SERVER['REQUEST_URI'], "md5") !== false){echo "class='active'";} ?>>MD5</a>
<a href='punycode' <?php if (strpos($_SERVER['REQUEST_URI'], "punycode") !== false){echo "class='active'";} ?>>Punycode</a>
<a href='rkn' <?php if (strpos($_SERVER['REQUEST_URI'], "rkn") !== false){echo "class='active'";} ?>>Check in block RKN</a>
<a href='telegram_tools' <?php if (strpos($_SERVER['REQUEST_URI'], "telegram_tools") !== false){echo "class='active'";} ?>>Telegram tools</a>
<a href='dwyou' <?php if (strpos($_SERVER['REQUEST_URI'], "dwyou") !== false){echo "class='active'";} ?>>Download Video Youtube</a>
</div>
