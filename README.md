# Utilities From LeakMind

Utilities from LeakMind (api.leakmind.one)

**System requirements:**
- Apache web server
- PHP 5.6+ (module GD)

The config.php file configures the parameters:
- test mode
- key
- captcha

Demo: [https://project.leakmind.one](https://project.leakmind.one)
