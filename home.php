<?php
require('./config.php');
if ($test_mode == 1) {
require('./head.php');
echo "<h2>TEST MODE</h2>";

$params=[
    "type"=>"test", 
    "key"=>$key, 
];
$ch = curl_init($api_url);
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
$result = curl_exec($ch);
curl_close($ch);
$dump = json_decode($result);

if (in_array(@$dump->{'error'}, $error)) {
    echo "Error: " . $dump->{'error'};
    require('./footer.php');
exit();
} elseif ($result == "test_request") {
    echo "request successful";
} 
else {
    echo $result ;
    }
require('./footer.php');
}
    elseif ($test_mode == 0) {
        require('./head.php');
        require('./config.php');
        echo "<h2>Utilities from LeakMind | $v</h2>";
        require('./footer.php');
    }
?>